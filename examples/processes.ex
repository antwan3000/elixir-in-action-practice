defmodule ChapterFive do
  def test_send_receive do
    send(self(), {:message, 1})

    receive_result = 
      receive do
        {:message, x} -> x + 2
      end
  end

  def collect_query_results do
    run_query = 
      fn query_def -> 
        Process.sleep(2000)
        "#{query_def} result"
      end

    async_query =
      fn query_def -> 
        caller = self()
        spawn(fn -> send(caller, {:query_result, run_query.(query_def)}) end)
      end

    Enum.each(1..5, &async_query.("query #{&1}"))

    get_result = 
      fn -> 
        receive do
         {:query_result, result} -> result
        end
      end

    results = Enum.map(1..5, fn _ -> get_result.() end)
  end
end
