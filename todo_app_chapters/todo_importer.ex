defmodule TodoEntry do
  defstruct id: nil, date: nil, title: nil

  def new(title, date \\ Date.utc_today) do
    %TodoEntry{title: title, date: date}
  end
end


defmodule TodoList do
  defstruct auto_id: 1, entries: %{}

  def new(entries \\ []) do
    Enum.reduce(
      entries,
      %TodoList{},
      &add_entry(&2, &1)
    )
  end

  def add_entry(todo_list, %TodoEntry{} = entry) do
    entry = Map.put(entry, :id, todo_list.auto_id)

    new_entries = Map.put(
      todo_list.entries,
      todo_list.auto_id,
      entry
    )

    %TodoList{todo_list | entries: new_entries, auto_id: todo_list.auto_id + 1}
  end

  def entries(todo_list, date, title_only \\ false) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> if title_only, do: entry.title, else: entry end)
  end

  def update_entry(todo_list, %TodoEntry{id: id} = new_entry) do
    put_in(todo_list.entries[id], new_entry)
    # update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  def update_entry(todo_list, entry_id, updater_fun) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error -> todo_list

      {:ok, old_entry} ->
        new_entry = updater_fun.(old_entry)
        new_entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %TodoList{todo_list | entries: new_entries}
    end
  end

  def delete_entry(todo_list, entry_id) when is_number(entry_id) do
    Map.delete(todo_list.entries, entry_id)
  end
end


defmodule TodoList.CsvImporter do
  def import(filename) do
    filename
    |> read_file
    |> create_todo_entries
    |> TodoList.new
  end

  defp read_file(filename) do
    File.stream!(filename)
    |> Stream.map(&String.replace(&1, "\n", ""))
  end

  defp create_todo_entries(lines) do
    lines
    |> Stream.map(&extract_fields/1)
    |> Stream.map(&create_todo/1)
  end

  defp extract_fields(line) do
    line
    |> String.split(",")
    |> convert_date
  end

  defp convert_date([date, title]) do
    {parse_date(date), title}
  end

  defp parse_date(date_string) do
    [year, month, day] = 
      String.split(date_string, "/")
      |> Enum.map(&String.to_integer(&1))
    {:ok, date} = Date.new(year, month, day)
    date
  end

  defp create_todo({date, title}) do
      TodoEntry.new(title, date) 
  end
end
